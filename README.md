# Pickemon
Dockerized microservice, based on Python3 and gRPC, for gathering information about Pokemon.


# Table Of Contents
- [Running](#running)
  - [Docker](#docker)
  - [Terminal](#terminal)
- [Test](#test)
- [Connect to Pickemon](#connect-to-pickemon)
- [Third-Party Services](#third-party-services)
- [Author](#author)
- [License](#license)


## Running
### Docker
By default, the containerized server runs at `0.0.0.0/6000`.
```shell
$ cd path_to/pickemon
pickemon$ docker build --tag pickemon .
pickemon$ docker run -p 6000:6000 pickemon
```


### Terminal
Clone Pickemon, and create a virtualenv in the Pickemon folder:
```shell
$ git clone https://gitlab.com/francescoracciatti/pickemon.git
$ python3 -m virtualenv path_to/pickemon
```

Activate the virtualenv:
```shell
$ cd path_to/pickemon
pickemon$ source bin/activate
```

Install Pickemon's requirements and dependencies:
```shell
(venv) pickemon$ pip3 install -r requirements.txt
(venv) pickemon$ pip3 install -e .
```

Move to `src` folder and compile the `proto` file :
```shell
(venv) pickemon/src$ python3 -m grpc_tools.protoc --proto_path=. pokemon.proto --python_out=.
```
It will create the file `pokemon_pb2.py`.

Then, run the server on a given host/port:
```shell
(venv) pickemon/src$ python3 server.py --host 127.0.0.1 --port 6000
 ```


# Test
Start the server, then:
```shell
(venv) pickemon/src$ python3 test_server.py --host 127.0.0.1 --port 6000
 ```
All tests must pass.


# Connect to Pickemon
You can connect to Pickemon via gRPC. 
The service is defined in the file [pokemon.proto](src/proto/pokemon.proto).


# Third-Party Services
Pickemon relies on [PokéApi](https://pokeapi.com).


## Author
Francesco Racciatti


## License
This project is licensed under the [MIT license](LICENSE).
