"""
This module provides a wrapper to get Pokemon from PokeAPI.
"""

from __future__ import annotations

import configparser
import logging
from abc import ABC
from typing import Dict, Any

import requests

from src.res.pokemon import Pokemon, PokemonNotFound
from src.utils.utils import replace_escape


class Configuration(object):
    """
    Wraps configuration parameters.
    """

    def __init__(self, language: str, version: str) -> None:
        """
        Initializes the configuration.

        :param language: the target language
        :param version: the target version
        """
        self.language = language
        self.version = version


def parse_config(filename: str) -> Configuration:
    """
    Parses the given configuration file.

    :param filename: the name of the configuration file
    :return: the parsed configuration parameters
    """
    config = configparser.ConfigParser()
    config.read(filename)

    language = config['resource.PokeAPI']['LANGUAGE']
    version = config['resource.PokeAPI']['VERSION']
    logging.info(f"PokeAPI configuration: language {language}, version {version}")
    return Configuration(language, version)


class DescriptionNotFound(RuntimeError):
    """
    Pokemon' description not found exception.
    """
    pass


class PokeAPI(ABC):
    """
    Wraps PokeAPI utilities.
    """

    ENDPOINT = "https://pokeapi.co/api/v2/pokemon-species"

    def __init__(self, configuration_filename: str) -> None:
        """
        Initializes the PokeAPI.

        :param configuration_filename: the configuration filename
        """
        self.configuration = parse_config(configuration_filename)

    def _build_from_json(self, json: Dict[Any, Any]) -> Pokemon:
        """
        Builds a Pokemon from the PokeAPI json response.

        :param json: the json containing the description of the Pokemon
        """
        name = json['name']
        habitat = json['habitat']['name']
        is_legendary = json['is_legendary']

        entries = json['flavor_text_entries']
        description = None
        for e in entries:
            # Looks for the current language and version
            if e['language']['name'].lower() == self.configuration.language \
                    and e['version']['name'].lower() == self.configuration.version:
                description = replace_escape(e['flavor_text'])

        if not description:
            raise DescriptionNotFound(f"Description of Pokemon {name} not found")

        return Pokemon(name, description, habitat, is_legendary)

    def get(self, name: str) -> Pokemon:
        """
        Gets the given Pokemon.

        :param name: the name of the Pokemon
        :return: the Pokemon
        """
        response = requests.get(f"{self.ENDPOINT}/{name}")
        if response.status_code != 200:
            logging.error(f"Cannot retrieve {name}, status code {response.status_code}")
            raise PokemonNotFound(f"Pokemon {name} not found")

        try:
            logging.debug(f"Pokemon {name} found")
            return self._build_from_json(response.json())
        except (DescriptionNotFound, KeyError, RuntimeError) as e:
            logging.error(f"Exception caught: {str(e)}")
            raise PokemonNotFound(f"Pokemon {name} not found")
