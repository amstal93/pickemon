"""
This module provides the implementation of the Pickemon microservice.
It retrieves information about Pokemon by exploiting api PokeAPI.
You can query Pickemon via gRPC requests, by using the name of the Pokemon.
"""

from __future__ import annotations

import argparse
import logging
from concurrent import futures

import grpc

from src.res.pokeapi import PokeAPI
from src.res.pokemon import PokemonNotFound

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.DEBUG)

protos, services = grpc.protos_and_services("proto/pokemon.proto")


class PokemonRetriever(services.PokemonRetrieverServicer):
    """
    Pokemon retrieval service.
    """

    def __init__(self, configuration_filename: str) -> None:
        """
        Initializes the retrieval services.

        :param configuration_filename: the name of the configuration file
        """
        self.public_api = PokeAPI(configuration_filename)

    def get(self, request, context):
        """
        Handles the request.

        :param request: the request containing the name of the Pokemon to look for
        :return:
        """
        try:
            pokemon = self.public_api.get(request.name)
            return protos.PokemonResponse(name=pokemon.name,
                                          description=pokemon.description,
                                          habitat=pokemon.habitat,
                                          is_legendary=pokemon.is_legendary)
        except PokemonNotFound:
            logging.error(f"Pokemon {request.name} not found")


def serve(server_params: ServerParams) -> None:
    """
    Runs the executors.

    :param server_params: the parameters to run the server
    :return:
    """
    logging.info("Starting server executors ...")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    services.add_PokemonRetrieverServicer_to_server(PokemonRetriever('config.ini'), server)
    server.add_insecure_port(f"{server_params.host}:{server_params.port}")
    server.start()
    server.wait_for_termination()


class ServerParams(object):
    """
    Wraps parameters to run the server.
    """

    def __init__(self, host: str, port: int) -> None:
        """
        Initializes the args wrapper.

        :param host: the host on which the server listens for incoming requests
        :param port: the port
        """
        self.host = host
        self.port = port


def parse_server_params() -> ServerParams:
    """
    Parse CLI arguments.

    :return: the parsed CLI arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', help='The host', required=True)
    parser.add_argument('--port', dest='port', help='The port', required=True)
    args = parser.parse_args()

    host = args.host
    port = int(args.port)
    logging.info(f"Got {host}/{port}")

    return ServerParams(host, port)


if __name__ == '__main__':
    logging.info("Pickemon is running ...")

    # Parses CLI arguments
    server_params = parse_server_params()

    # Starts the Pokemon retrieval executors
    serve(server_params)

    logging.info("Pickemon ended")
